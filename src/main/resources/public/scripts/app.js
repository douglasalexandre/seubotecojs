

/**
 * Created by bluesprogrammer on 26/07/15.
 */

var app = angular.module('seubotecojs', [
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'ngRoute'
]);

app.config(function ($routeProvider) {
    $routeProvider.when('/', {
        templateUrl: 'views/list.html',
        controller: 'ListCtrl'
    }).when('/novo', {
        templateUrl: 'views/novo.html',
        controller: 'CreateCtrl'
    }).otherwise({
        redirectTo: '/'
    })
});

app.controller('ListCtrl', function ($scope, $http) {
    $http.get('/api/v1/produtos').success(function (data) {
        $scope.produtos = data;
    }).error(function (data, status) {
        console.log('Error ' + data)
    })
});

app.controller('CreateCtrl', function ($scope, $http, $location) {
    
    $scope.createProduto = function () {
        console.log($scope.produto);
        $http.post('/api/v1/produtos', $scope.produto).success(function (data) {
            $location.path('/');
        }).error(function (data, status) {
            console.log('Error ' + data)
        })
    }
});

app.controller('UpdateCtrl', function ($scope, $http, $location){
    $scope.updateProduto = function (){
        console.log($scope.produto);
        $http.put('produtos/:id', $scope.produto).success(function (data){
            $location.path('/:id');
        }).error(function (data, status){
            console.log('Error' + data)
        })
    }
});

app.controller('EditCtrl', function(){
    $scope.editProduto = function(id) {
        $scope.produto.nome = $scope.produtos[id-1].produto.nome;
        $scope.produto.categoria = $scope.produtos[id-1].produto.categoria; 
        $scope.produto.tipoDaBebida = $scope.produtos[id-1].produto.preco;
        $scope.produto.descricao = $scope.produtos[id-1].produto.descricao;
        $scope.produto.preco = $scope.produtos[id-1].produto.preco;
        $scope.produto.gramatura = $scope.produtos[id-1].produto.gramatura;
        $scope.produto.promocao = $scope.produtos[id-1].produto.promocao;
        $scope.produto.imageUrl = $scope.produtos[id-1].produto.imageUrl;    
    }
});