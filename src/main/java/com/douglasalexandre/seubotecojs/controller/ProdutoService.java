/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.douglasalexandre.seubotecojs.controller;

import com.douglasalexandre.seubotecojs.model.Produto;
import com.google.gson.Gson;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.bson.types.ObjectId;

/**
 *
 * @author bluesprogrammer
 */
public class ProdutoService {
    
    private final DB db;
    private final DBCollection collection;

    public ProdutoService(DB db) {
        this.db = db;
        this.collection = db.getCollection("cardapioSB");
    }

    public List<Produto> findAll() {
        List<Produto> produtos = new ArrayList<>();
        DBCursor dbObjects = collection.find();
        while (dbObjects.hasNext()) {
            DBObject dbObject = dbObjects.next();
            produtos.add(new Produto((BasicDBObject) dbObject));
        }
        return produtos;
    }

    public void createNewProduto(String body) {
        Produto produto = new Gson().fromJson(body, Produto.class);
        collection.insert(new BasicDBObject("nome", produto.getNome())
                .append("categoria", produto.getCategoria())
                .append("tipoDaBebida", produto.getTipoDaBebida())
                .append("descricao", produto.getDescricao())
                .append("preco", produto.getPreco())
                .append("promocao", produto.isPromocao())
                .append("imageUrl", produto.getImageUrl())
                .append("gramatura", produto.getGramatura())
                .append("versaoDB", produto.getVersaoDB())
                .append("createdOn", new Date()));
    }

    public Produto find(String id) {
        return new Produto((BasicDBObject) collection.findOne(new BasicDBObject("_id", new ObjectId(id))));
    }

    public Produto update(String produtoId, String body) {
        Produto produto = new Gson().fromJson(body, Produto.class);
        collection.update(new BasicDBObject("_id", new ObjectId(produtoId)), new BasicDBObject("$set", new BasicDBObject("createdOn", produto.getCreatedOn())));
        return this.find(produtoId);
    }
    
}
