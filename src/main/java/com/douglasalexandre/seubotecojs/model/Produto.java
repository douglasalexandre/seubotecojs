/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.douglasalexandre.seubotecojs.model;

import com.mongodb.BasicDBObject;
import java.util.Date;
import org.bson.types.ObjectId;

/**
 *
 * @author bluesprogrammer
 */
public class Produto{ 

    private String id;
    
    private String nome;
    
    private String categoria;
    
    private String tipoDaBebida;

    private String descricao;
    
    private Double preco;
     
    private boolean promocao;

    private String imageUrl;
    
    private String gramatura;
    
    private int versaoDB;
    
    private Date createdOn = new Date();

    public Produto(BasicDBObject dbObject) {
        this.id = ((ObjectId) dbObject.get("_id")).toString();
        this.nome = dbObject.getString("nome");
        this.categoria = dbObject.getString("categoria");
        this.tipoDaBebida = dbObject.getString("tipoDaBebida");
        this.descricao = dbObject.getString("descricao");
        this.preco = dbObject.getDouble("preco");
        this.promocao = dbObject.getBoolean("promocao");
        this.imageUrl = dbObject.getString("imageUrl");
        this.gramatura = dbObject.getString("gramatura");
        this.versaoDB = dbObject.getInt("versaoDB");
        this.createdOn = dbObject.getDate("createdOn");
    }

    public String getNome() {
        return nome;
    }

    public String getCategoria() {
        return categoria;
    }

    public String getTipoDaBebida() {
        return tipoDaBebida;
    }

    public String getDescricao() {
        return descricao;
    }

    public Double getPreco() {
        return preco;
    }

    public boolean isPromocao() {
        return promocao;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public String getGramatura() {
        return gramatura;
    }

    public int getVersaoDB() {
        return versaoDB;
    }

    public Date getCreatedOn() {
        return createdOn;
    }  
}
