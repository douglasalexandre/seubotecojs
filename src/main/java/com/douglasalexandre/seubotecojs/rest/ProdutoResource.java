/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.douglasalexandre.seubotecojs.rest;

import com.douglasalexandre.seubotecojs.controller.JsonTransformer;
import com.douglasalexandre.seubotecojs.controller.ProdutoService;
import static spark.Spark.get;
import static spark.Spark.post;
import static spark.Spark.put;

/**
 *
 * @author bluesprogrammer
 */
public class ProdutoResource {
    
    private static final String API_CONTEXT = "/api/v1";

    private final ProdutoService produtoService;

    public ProdutoResource(ProdutoService produtoService) {
        this.produtoService = produtoService;
        setupEndpoints();
    }

    private void setupEndpoints() {
        post(API_CONTEXT + "/produtos", "application/json", (request, response) -> {
            produtoService.createNewProduto(request.body());
            response.status(201);
            return response;
        }, new JsonTransformer());

        get(API_CONTEXT + "/produtos/:id", "application/json", (request, response)

                -> produtoService.find(request.params(":id")), new JsonTransformer());

        get(API_CONTEXT + "/produtos", "application/json", (request, response)

                -> produtoService.findAll(), new JsonTransformer());

        put(API_CONTEXT + "/produtos/:id", "application/json", (request, response)

                -> produtoService.update(request.params(":id"), request.body()), new JsonTransformer());
    }
}
